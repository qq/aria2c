#!/bin/bash
CONF_PATH=/usr/local/aria2/etc/aria2.conf
if [ -f ${CONF_PATH} ]; then
    list=`curl https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all.txt|awk NF|sed ":a;N;s/\n/,/g;ta"`
    if [ -z "`grep "bt-tracker" ${CONF_PATH}`" ]; then
        sed -i '$a bt-tracker='${list} ${CONF_PATH}
        echo "Add tracker done."
    else
        sed -i "s@bt-tracker.*@bt-tracker=$list@g" ${CONF_PATH}
        echo "Update tracker done."
    fi
else
    echo "No Config File Found."
fi
