# Version: 1.0.0
# Base info, must start with FROM
FROM centos 
#ARG
MAINTAINER ktaog6 ktaog6@126.com
LABEL version="1.0.0" location="CN"
ENV REFRESHED_AT 2018-12-19
ENV HOME "/root/"

# Run command when build
RUN ["/bin/bash"]
WORKDIR $HOME
RUN yum -y install tmux git wget file cppunit autoconf automake make gcc gcc-c++ libtool cppunit-devel libxml2-devel libgcrypt-devel openssl-devel nettle-devel gmp-devel libssh2-devel c-ares-devel zlib-devel sqlite-devel expat-devel gettext-devel openssl httpd
RUN ["ldconfig"]
RUN ["git", "clone", "https://github.com/aria2/aria2.git"]
WORKDIR "aria2"
RUN ["autoreconf", "-i"]
RUN ["./configure", "--prefix=/usr/local/aria2/", "--with-ca-bundle=/etc/ssl/certs/ca-bundle.crt", "--without-gnutls", "--with-openssl"]
RUN ["make"]
RUN ["make", "install"]
RUN ["mkdir", "/usr/local/aria2/etc"]
RUN ["mkdir", "/usr/local/aria2/var"]
RUN ["mkdir", "/usr/local/aria2/downloads"]
ADD aria2cd.sh /usr/local/aria2/bin/
ADD settracker.sh /usr/local/aria2/bin/
ADD aria2.conf /usr/local/aria2/etc/
RUN touch /usr/local/aria2/var/aria2.session
RUN ["chmod", "a+x", "/usr/local/aria2/bin/aria2cd.sh"]
RUN ["chmod", "a+x", "/usr/local/aria2/bin/settracker.sh"]
WORKDIR $HOME
RUN ["rm", "-rf", "aria2"]

WORKDIR $HOME
RUN ["git", "clone", "https://github.com/ziahamza/webui-aria2.git"]
RUN ["cp", "-a", "webui-aria2/docs", "/var/www/html/aria2c"]
RUN ["rm", "-rf", "webui-aria2"]

WORKDIR $HOME
RUN ["ln", "-s", "/usr/local/aria2/downloads", "/var/www/html/"]
RUN ln -s /var/www/html $HOME
RUN ln -s /usr/local/aria2/bin/aria2cd.sh $HOME

# Execute command when os start, can be replace by docker run
CMD ["/bin/bash"]

# port used
EXPOSE 80
EXPOSE 6800

#STOPSIGNAL
